#SocMechanicsFactory



## Install

```sh
$ npm install --save @adwatch/socmechanics-factory
```


## Usage

```js
import SocMechanicsFactory from '@adwatch/socmechanics-factory';
// or
var SocMechanicsFactory = require('@adwatch/form/build');

//or you can use separate Social Network
import {SocMechanicsVK,
        SocMechanicsFB,
        SocMechanicsOK,
        SocMechanicsTW,
        SocMechanicsIN} from '@adwatch/socmechanics-factory';
```


## API - SocMechanicsFactory

Initialize SocMechanicsFactory

```js
const socMechanicsFactory = new SocMechanicsFactory();
```

####produce(type, config)

Initialize SocMechanicsFactory

```js
socMechanicsFactory.produce('VK', {appId: 5878932});
```



## API - SocMechanics[VK,FB,OK,TW,IN]

For instance VK type

Initialize SocMechanicsVK

```js
const socMechanicsVK = new SocMechanicsVK({appId: 5878932});
```


####init()

Initialization

Allowed for all networks

```js
socMechanicsVK.init();
```


####getUserAuth()

Get user Auth data

This method must be use at first.

Allowed for all networks

Return Promise

```js
socMechanicsVK.getUserAuth();
```


####getUserInfo()

Get Main info of user

Only (VK,FB,OK)

Return Promise

```js
socMechanicsVK.getUserInfo();
```


####getUserAvatar()

Get Avatar Profile of user

Allowed for all networks

Return Promise

```js
socMechanicsVK.getUserAvatar();
```


####getUserPost()

Get User Posts

Only (VK,FB,OK)

Return Promise

```js
socMechanicsVK.getUserPost();
```


####getLastPost(key)

Get Last post from cache

Only (VK,FB,OK)

```js
socMechanicsVK.getLastPost('timeStamp');
```


####makePackage(CASE)

Make package with filtered data

About CASEs you can follow to https://www.npmjs.com/package/@adwatch/socmechanics-adapter

Allowed for all networks

```js
socMechanicsVK.makePackage('freeShare:onePost:allNetworks');
```


####checkWallPost(config)

Check conformity last post data with your config data

You can to see most common information to  https://www.npmjs.com/package/@adwatch/share

Only (VK,FB,OK)

Return true/false

```js
socMechanicsVK.checkWallPost({
    description: 'Test page desc',
    img: 'http://example.ru/img/share.jpg',
    title: 'Test Share',
    url: window.location.href,
    workId: 2
});
```


 ## License

 MIT ©
