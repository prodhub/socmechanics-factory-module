'use strict';

var md5 = require('js-md5');
var axios = require('axios');
var $ = require('jquery');

const _protect = {
    openPopup(url){
        const w = 640,
            h = 400,
            left = ( screen.width / 2 ) - ( w / 2 ),
            top = ( screen.height / 2 ) - ( h / 2 );

        const win = window.open(url, 'Авторизация', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        return win;
    }
};

class SocMechanicsFactory{

    constructor(){
        this.types = {
            'VK': (config) => {
                return new SocMechanicsVK(config);
            },
            'FB': (config) => {
                return new SocMechanicsFB(config);
            },
            'OK': (config) => {
                return new SocMechanicsOK(config);
            },
            'TW': (config) => {
                return new SocMechanicsTW(config);
            },
            'IN': (config) => {
                return new SocMechanicsIN(config);
            }
        }
    }

    produce(type, config){
        if(this.types[type]){
            return this.types[type](config);
        }else{
            throw TypeError(`No method ${type}`);
        }
    }

}


class SocMechanicsVK {
    constructor(config){
        this.appId = config.appId;
    }

    getUserAuth(){
        let that = this;
        let promise = new Promise((resolve, reject) => {
            VK.Auth.login(function(res) {
                if(res.session == null || res.status == 'unknown' || res.status == 'not_authorized'){
                    that.auth = false;
                    reject(false);
                }else{
                    that.auth = res;
                    resolve(that.auth);
                }
            });
        });

        return promise;
    }

    getUserInfo(){
        let that = this;
        let promise = new Promise((resolve, reject) => {
            VK.Api.call('users.get', {}, res => {
                let user = res.response[0];

                if(user) {
                    that.user = user;
                    resolve(that.user);
                }else{
                    that.user = false;
                    reject(false);
                }
            });
        });

        return promise;
    }

    getUserAvatar(){
        let that = this;
        return new Promise((resolve, reject) => {
            VK.Api.call('users.get', {fields: 'photo_200'}, res => {
                let user = res.response[0];

                if(user) {
                    that.user = user;
                    resolve(that.user.photo_200);
                }else{
                    that.user = false;
                    reject('Don`t get avatar');
                }
            });
        });
    }

    getUserPost(options){
        let that = this,
            defaultOptions = {
                owner_id: that.user.uid
            };

        if(options){
            for(var i in options){
                defaultOptions[i] = options[i];
            }
        }

        let promise = new Promise((resolve, reject) => {
            VK.Api.call(
                'wall.get',
                defaultOptions,
                res => {
                    if(res){
                        that.post = res;
                        resolve(that.post);
                    }else{
                        reject('Don`t get post');
                    }
                }
            );
        });

        return promise;
    }

    getLastPost(key){
        let post = this.post.response[1];

        if(key){
            switch(key){
                case 'timeStamp':
                    return post.date;
                    break;

                default:
                    break;
            }
        }

        return post;
    }

    makePackage(CASE){
        switch(CASE){
            case 'freeShare:onePost:allNetworks':{
                return {
                    userId: this.user.uid,
                    postId: this.getLastPost().id,
                    networkType: 'vk'
                }
            }
            case 'getAvatar:allNetworks':{
                return {
                    userId: this.user.uid,
                    userAvatar: this.user.photo_200,
                    networkType: 'vk'
                }
            }
            default:
                return {}
        }
    }

    checkWallPost(infoShare){
        let post = this.getLastPost().attachment;

        if(post && post.type == 'link'){
            let	type = post.type,
                link = post.link,
                url = link.url;

            if((type === 'link') && (url === infoShare.url)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    init(){
        let that = this;
        VK.init({
            apiId: 	that.appId
        });
    }
}



class SocMechanicsFB{
    constructor(config){
        this.privacy = 'EVERYONE';
        this.appId = config.appId;
    }

    getUserAuth(){
        let that = this;

        let promise = new Promise((resolve, reject) => {
            FB.login( res => {
                if(res.status == 'connected' && res.authResponse.userID){
                    that.auth = res;
                    resolve(that.auth);
                }else{
                    that.auth = false;
                    reject(false);
                }
            }, {scope: 'user_posts'});
        });

        return promise;
    }

    getUserInfo(){
        let that = this;
        let promise = new Promise((resolve, reject) => {
            FB.getLoginStatus( res => {
                let user = res;

                if(user) {
                    that.user = user;
                    resolve(that.user);
                }else{
                    that.user = false;
                    reject(false);
                }
            });
        });

        return promise;
    }

    getUserAvatar(){
        let that = this;
        return new Promise((resolve, reject) => {
            FB.api(
                `/${that.auth.authResponse.userID}/picture`,
                {redirect:false,height:200,width:200},
                res => {
                    if(res){
                        if(res.data){
                            if(!that.user){that.user = {}}
                            that.user.avatar = res.data.url;
                            resolve(that.user.avatar);
                        }
                    }else{
                        reject('Don`t get avatar');
                    }
                }
            );
        });
    }

    getUserPost(options){
        var that = this,
            defaultOptions = ['fields=id', 'link', 'privacy', 'name', 'created_time'];

        if(options){
            defaultOptions = [];
            for(var i = 0; i < options.length; i++){
                defaultOptions.push(options[i]);
            }
        }

        defaultOptions = defaultOptions.join(',');

        let promise = new Promise((resolve, reject) => {
            FB.api(
                `/me/posts?${defaultOptions}`,
                {limit: 1},
                res => {
                    if(res){
                        if(res.data.length){
                            that.post = res.data;
                            resolve(that.post);
                        }else{
                            reject('PrivateSettingsError');
                        }
                    }else{
                        reject('Don`t get post');
                    }
                }
            );
        });

        return promise;
    }

    getLastPost(key){
        let post = this.post[0];

        if(key){
            switch(key){
                case 'timeStamp':
                    return Date.parse(post.created_time)/1000;
                    break;

                default:
                    break;
            }
        }

        return post;
    }

    makePackage(CASE){
        switch(CASE){
            case 'freeShare:onePost:allNetworks':{
                return {
                    userId: this.user.authResponse.userID,
                    postId: this.getLastPost().id,
                    networkType: 'fb'
                }
            }
            case 'getAvatar:allNetworks':{
                return {
                    userId: this.auth.authResponse.userID,
                    userAvatar: this.user.avatar,
                    networkType: 'fb'
                }
            }
            default:
                return {}
        }
    }

    checkWallPost(infoShare){
        let post = this.getLastPost();

        if(post){
            let	type = post.link || null,
                url = post.link,
                privacy = post.privacy.value;

            if((type) && (url === infoShare.url) && (privacy === this.privacy)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    init(){
        let that = this;
        window.fbAsyncInit = function() {
            FB.init({
                appId      : `${that.appId}`,
                xfbml      : true,
                version    : 'v2.8'
            });
            FB.AppEvents.logPageView();
        };
    }
}



class SocMechanicsOK {
    constructor(config){
        this.appId = config.appId;
        this.app_key = config.app_key;
        this.app_secret_key = config.app_secret_key;
        this.url = location.origin;
        this.api = 'https://api.ok.ru/fb.do?';
    }

    getSignature(string){
        return md5(string);
    }

    getUserAuth(){
        let that = this,
            authPopup = _protect.openPopup(`https://connect.ok.ru/oauth/authorize?client_id=${this.appId}&scope=VALUABLE_ACCESS&response_type=token&redirect_uri=${this.url}`),
            int = setInterval(()=>{
                try{
                    if(authPopup.location.origin == that.url || authPopup.location.hash.indexOf('error') !== -1){
                        var hash = OKSDK.Util.getRequestParameters(authPopup.location.hash);

                        authPopup.opener.postMessage(JSON.stringify({
                            "data":hash,
                            "hash":authPopup.location.hash
                        }), "*");

                        authPopup.close();
                        clearInterval(int);
                    }
                }catch(e){}
            }, 100);


        let promise = new Promise((resolve, reject) => {
            window.addEventListener('message', (e)=>{
                if(!(/_FB_/gi.test(e.data))){
                    try{
                        let msg = JSON.parse(e.data),
                            {data, hash} = msg;

                        if(data.access_token){
                            that.auth = data;
                            resolve(data);
                        }else{
                            that.auth = false;
                            reject('false');
                        }
                    }catch(e){}
                }
            }, false);
        });

        return promise;
    }

    getUserInfo(){
        let that = this;

        const method = 'users.getCurrentUser';

        let sig = that.getSignature(`application_key=${that.app_key}method=${method}${that.auth.session_secret_key}`),
            promise = new Promise((resolve, reject) => {
                axios.get(`${that.api}application_key=${that.app_key}&method=${method}&access_token=${that.auth.access_token}&sig=${sig}`)
                    .then((res)=>{
                        let data = res.data;

                        if(!data.error_code || !data.error_msg){
                            that.user = data;
                            that.checkLocked();
                            resolve(data);
                        }
                    })
                    .catch((err)=>{
                        reject(err);
                    });
            });

        return promise;
    }

    checkLocked(){
        let that = this;

        const method = 'users.getInfo';
        const fields = 'user.SHOW_LOCK';

        let sig = that.getSignature(`application_key=${that.app_key}fields=${fields}method=${method}uids=${that.user.uid}${that.auth.session_secret_key}`);
        return new Promise((resolve, reject) => {
            axios.get(`${that.api}application_key=${that.app_key}&fields=${fields}&uids=${that.user.uid}&method=${method}&access_token=${that.auth.access_token}&sig=${sig}`)
                .then((res)=>{
                    let show_lock = res.data[0].show_lock;
                    that.user.show_lock = show_lock;
                    resolve(show_lock);
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    }

    getUserPost(options){
        let that = this;

        const fields = 'feed.POST_REFS,' +
            'feed.DATE, feed.DATE_MS,' +
            'media_topic.ID,' +
            'media_topic.MEDIA,' +
            'media_topic.MEDIA_DESCRIPTION,' +
            'media_topic.PUBLICATION_DATE_MS,' +
            'media_topic.MEDIA_TITLE,' +
            'media_topic.MEDIA_TYPE,' +
            'media_topic.MEDIA_URL';
        const method = 'stream.get';
        const patterns = 'POST';

        let sig = that.getSignature(`application_key=${that.app_key}fields=${fields}method=${method}patterns=${patterns}uid=${that.user.uid}${that.auth.session_secret_key}`),
            promise = new Promise((resolve, reject) => {
                setTimeout(()=>{
                    axios.get(`${that.api}application_key=${that.app_key}&fields=${fields}&patterns=${patterns}&uid=${that.user.uid}&method=${method}&access_token=${that.auth.access_token}&sig=${sig}`)
                        .then((res)=>{
                            let data = res.data.entities;

                            if(data && (!data.error_code || !data.error_msg)){
                                let lastPost = Object.assign({}, data.media_topics[0], {date: res.data.feeds[0].date_ms});
                                that.post = lastPost;
                                resolve(data);
                            }
                        })
                        .catch((err)=>{
                            reject(err);
                        });
                },1500);
            });

        return promise;
    }

    getLastPost(key){
        let post = this.post;

        if(key){
            switch(key){
                case 'timeStamp':
                    return Math.round(post.date/1000);
                    break;

                default:
                    break;
            }
        }

        return post;
    }

    makePackage(CASE){
        switch(CASE){
            case 'freeShare:onePost:allNetworks':{
                return {
                    userId: this.user.uid,
                    postId: this.getLastPost().id,
                    networkType: 'ok'
                }
            }
            case 'getAvatar:allNetworks':{
                return {
                    userId: this.user.uid,
                    userAvatar: this.user.pic_3,
                    networkType: 'ok'
                }
            }
            default:
                return {}
        }
    }

    checkWallPost(infoShare){
        let post = this.getLastPost().media;
        let filterPost;

        post.forEach(elem=>{
            if(elem.type == 'link' && elem.url){
                filterPost = elem;
            }
        });


        if(filterPost){
            let	type = filterPost.type,
                url = filterPost.url;

            if((type === 'link') && (url === infoShare.url) && (!this.user.show_lock)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    init(){

    }
}


class SocMechanicsTW{
    constructor(config){
        this.proxy = config.proxy;
    }

    customizeImg(str, path){
        return str.replace(path, '');
    }

    getUserAuth(){
        let that = this;

        let authPopup = _protect.openPopup(`${this.proxy}`),
            int = setInterval(()=>{
                try{
                    if(/oauth_verifier/.test(authPopup.location.href)){

                        let key = 'oauth_verifier=',
                            href = authPopup.location.href,
                            oauthVerifier = href.slice(href.indexOf(key)+key.length),
                            data = authPopup.document.getElementById('info').innerHTML;

                        authPopup.opener.postMessage(JSON.stringify({
                            oauthVerifier,
                            data
                        }), "*");

                        authPopup.close();
                        clearInterval(int);
                    }
                }catch(e){}
            }, 2000);


        let promise = new Promise((resolve, reject) => {
            window.addEventListener('message', (e)=>{
                if(!(/_FB_/gi.test(e.data))){
                    try{
                        let msg = JSON.parse(e.data),
                            {oauthVerifier, data} = msg;

                        if(oauthVerifier){
                            that.auth = {oauthVerifier};
                            that.user = JSON.parse(data);
                            resolve(msg);
                        }else{
                            that.auth = false;
                            reject('false');
                        }
                    }catch(e){}
                }
            }, false);
        });

        return promise;
    }

    getUserAvatar(){
        let that = this;

        return new Promise((resolve, reject)=>{
            resolve(that.customizeImg(that.user.profile_image_url, '_normal'));
        });
    }

    makePackage(CASE){
        switch(CASE){
            case 'getAvatar:allNetworks':{
                return {
                    userId: this.user.id,
                    userAvatar: this.customizeImg(this.user.profile_image_url, '_normal'),
                    networkType: 'tw'
                }
            }
            default:
                return {}
        }
    }

    init(){

    }
}


class SocMechanicsIN{
    constructor(config){
        this.appId = config.appId;
        this.url = location.href;
        this.originUrl = location.origin;
    }

    getUserAuth(){
        let that = this,
            authPopup = _protect.openPopup(`https://api.instagram.com/oauth/authorize/?client_id=${this.appId}&redirect_uri=${this.url}&response_type=token`),
            int = setInterval(()=>{
                try{
                    if(authPopup.location == that.originUrl || /access_token/.test(authPopup.location.hash)){

                        let key = '#access_token=',
                            href = authPopup.location.href,
                            accessToken = href.slice(href.indexOf(key)+key.length);

                        authPopup.opener.postMessage(JSON.stringify({
                            accessToken
                        }), "*");

                        authPopup.close();
                        clearInterval(int);
                    }
                }catch(e){}
            }, 100);


        let promise = new Promise((resolve, reject) => {
            window.addEventListener('message', (e)=>{
                if(!(/_FB_/gi.test(e.data))){
                    try{
                        let msg = JSON.parse(e.data),
                            {accessToken} = msg;

                        if(accessToken){
                            that.auth = {accessToken};
                            resolve(msg);
                        }else{
                            that.auth = false;
                            reject('false');
                        }
                    }catch(e){}
                }
            }, false);
        });

        return promise;
    }

    getUserAvatar(){
        let that = this;
        const method = '/users/self';

        return new Promise((resolve, reject)=>{

            $.ajax({
                url: `https://api.instagram.com/v1${method}`,
                dataType: 'jsonp',
                type: 'GET',
                data: {access_token: that.auth.accessToken},
                success: function(res){
                    let {meta:{code}, data} = res;

                    if(code == 200){
                        that.user = data;
                        resolve(data.profile_picture);
                    }else{
                        reject(res);
                    }
                },
                error: function(err){
                    reject(err)
                }
            });

        });
    }

    makePackage(CASE){
        switch(CASE){
            case 'getAvatar:allNetworks':{
                return {
                    userId: this.user.id,
                    userAvatar: this.user.profile_picture,
                    networkType: 'in'
                }
            }
            default:
                return {}
        }
    }

    init(){

    }
}

module.exports = {
    SocMechanicsFactory: SocMechanicsFactory,
    SocMechanicsVK: SocMechanicsVK,
    SocMechanicsFB: SocMechanicsFB,
    SocMechanicsOK: SocMechanicsOK,
    SocMechanicsTW: SocMechanicsTW,
    SocMechanicsIN: SocMechanicsIN
};